import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewRaceSetupPage } from './new-race-setup.page';

describe('NewRaceSetupPage', () => {
  let component: NewRaceSetupPage;
  let fixture: ComponentFixture<NewRaceSetupPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewRaceSetupPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewRaceSetupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
