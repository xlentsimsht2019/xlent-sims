import { Component, OnInit } from "@angular/core";
import {
  StorageMenagerService,
  Race
} from "../services/storage-menager.service";
import { Platform, ToastController, AlertController } from "@ionic/angular";
import { FileMenagerService } from "../services/file-menager.service";
import { Router, NavigationEnd } from "@angular/router";

@Component({
  selector: "app-new-race-setup",
  templateUrl: "./new-race-setup.page.html",
  styleUrls: ["./new-race-setup.page.scss"]
})
export class NewRaceSetupPage implements OnInit {
  races: Race[] = [];
  path: string;
  race: Race = {
    name: "",
    id: Date.now(),
    interval: 1,
    raceType: "individual",
    finishflag: false,
    numberOfRunners: 1
  };

  // tslint:disable-next-line: ban-types
  minDate: any = new Date().toISOString();
  maxDate: any = new Date(
    new Date().setFullYear(new Date().getFullYear() + 1)
  ).toISOString();

  constructor(
    private storageService: StorageMenagerService,
    private plt: Platform,
    private toastController: ToastController,
    private alertController: AlertController,
    private fileMenager: FileMenagerService,
    private router: Router
  ) {}

  updateInterval() {
    const type = this.race.raceType;
    if (type === "mass") {
      this.race.interval = 0;
    }
    if (type === "individual") {
      this.race.interval = 1;
    }
  }

  async addRace() {
    let canSave = true;
    let idRace = 0;
    let numRunners = 1;
    let typeRace = this.race.raceType;
    const raceID = this.race.id;
    const numberOfRunners = this.race.numberOfRunners;

    console.log("ID: " + raceID);
    console.log("Nº Runners: " + numberOfRunners);
    // I check that everything is correct
    console.log("Check all races value");

    // Check Name
    if (this.race.name.length === 0) {
      // error name
      canSave = false;
      console.log("Name Error");
      this.presentErrorAlert(
        "Name Required",
        "Race has to have a name",
        "Enter a name for the race",
        ["OK"]
      );
    } else if (!this.race.date) {
      // error date
      canSave = false;
      console.log("Date Error");
      this.presentErrorAlert(
        "Date Required",
        "Race has to have a departure date",
        "Enter a correct date for the race",
        ["OK"]
      );
    }

    if (this.race.raceType !== "individual") {
      this.race.interval = 0;
    }

    if (this.race.numberOfRunners < 0) {
      canSave = false;
      console.log("Number of Runners Error");
      this.presentErrorAlert(
        "Number of Runners Required",
        "Race has to have at least one runner",
        "Enter valid number of runners",
        ["OK"]
      );
    }

    if (canSave) {
      console.log(this.race);

      this.race.id = Date.now();
      this.race.file = this.race.id + ".sav";

      idRace = this.race.id;
      numRunners = this.race.numberOfRunners;
      typeRace = this.race.raceType;

      idRace = this.race.id;
      numRunners = this.race.numberOfRunners;
      // We save the race
      //await this.plt.ready();
      this.storageService.addRace(this.race).then(res => {
        this.fileMenager.createNewFile(this.race.file);
        this.showToast("Race Added Succesfully!");
        console.log("preswap page");
        this.router.navigate([
          "newracelist",
          { race: JSON.stringify(this.race) }
        ]);
        this.race = <Race>{};
      });
    } else {
      this.presentErrorAlert(
        "Error Saving Race",
        "An error occurred while saving the race",
        "Try it again in few minutes",
        ["OK"]
      );
    }
  }

  alert() {
    this.presentErrorAlert(
      "No race information has saved yet",
      "Are you sure to delet all the inserted info? ",
      "",
      [
        {
          text: "OK",
          handler: () => {
            this.router.navigate(["/home"]);
          }
        },
        { text: "Cancel", role: "cancel" }
      ]
    );
  }

  async showToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 1500
    });
    toast.present();
  }

  async presentErrorAlert(title, subtitle, msg, buttons) {
    const alert = await this.alertController.create({
      header: title,
      subHeader: subtitle,
      message: msg,
      buttons
    });
    await alert.present();
  }

  ngOnInit() {
    this.minDate = new Date().toISOString();
  }
}
