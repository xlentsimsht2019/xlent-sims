import {
  FileMenagerService,
  RunnersRow
} from "../services/file-menager.service";
import { NewRaceSetupPage } from "../new-race-setup/new-race-setup.page";
import {
  StorageMenagerService,
  Race
} from "../services/storage-menager.service";
import { Platform } from "@ionic/angular";
import { ActivatedRoute } from "@angular/router";
//import { writeFile } from 'fs';
import { Router, NavigationEnd } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { stringify } from "querystring";
import { TargetLocator } from "selenium-webdriver";
//import { IonReorderGroup } from '@ionic/angular';

@Component({
  selector: "app-new-race-list",
  templateUrl: "./new-race-list.page.html",
  styleUrls: ["./new-race-list.page.scss"]
})
export class NewRaceListPage implements OnInit {
  listItems: any;
  listFor: any;
  listIntervals: any;
  runners: RunnersRow[] = [];
  runner: RunnersRow;
  raceinfo: Race;

  type: string;
  date: any;
  intervalTime: any;
  participants: any;
  //filename = Date.now() + ".sav";
  //listNumbers: string[];
  //listNames: string[];

  constructor(
    private file: FileMenagerService,
    private storageService: StorageMenagerService,
    private plt: Platform,
    private fileMenager: FileMenagerService,
    private activeroute: ActivatedRoute,
    private router: Router
  ) {
    this.raceinfo = JSON.parse(this.activeroute.snapshot.paramMap.get("race"));
    console.log("NewRaceList: raceinfo");
    console.log(this.raceinfo);

    this.type = this.raceinfo.raceType;
    this.intervalTime = Number(this.raceinfo.interval); // Needs to be specified as Number
    this.participants = Number(this.raceinfo.numberOfRunners); // Needs to be specified as Number
    this.date = String(this.raceinfo.date);
  }

  // Temporary constructors for names and numbers
  /*this.listNames = [
      "Juan Garcia",
      "Paolo Rossi",
      "Karl Karlsson"
    ];
    this.listNumbers = [
      "18",
      "99",
      "34"
    ];*/
  createInterval() {
    // The date string is split up and converted from string to number
    //var date = "2014-01-01 10:08:55";
    var t = this.date.split(/[- : T +]/);
    var d = new Date(
      parseInt(t[0]),
      parseInt(t[1]) - 1,
      parseInt(t[2]),
      parseInt(t[3]),
      parseInt(t[4]),
      parseInt(t[5]),
      parseInt(t[6])
    );
    var intervalToDate = new Date(d);
    this.listIntervals = [];

    // The intervals are created and formatted in "HH:MM:SS:mSmS"
    for (let i = 0; i < this.participants; i++) {
      var newInterval =
        (intervalToDate.getHours() < 10 ? "0" : "") + //checks if seconds need a leading 0
        intervalToDate.getHours() +
        ":" +
        (intervalToDate.getMinutes() < 10 ? "0" : "") +
        intervalToDate.getMinutes() +
        ":" +
        (intervalToDate.getSeconds() < 10 ? "0" : "") +
        intervalToDate.getSeconds() +
        "." +
        0 +
        0;

      this.listIntervals.push(newInterval);
      // Only increments if type is interval
      if (this.type == "individual") {
        intervalToDate.setSeconds(
          intervalToDate.getSeconds() + this.intervalTime
        );
      }
    }
    console.table(this.listIntervals);
  }

  createList() {
    this.listItems = [];
    this.listFor = [];

    // Inserts the names and numbers into listItems
    for (let i = 0; i < this.participants; i++) {
      this.listFor = [
        {
          string: "",
          number: ""
        }
      ];
      //let writeFile = function(x, y) { return x + y; };

      this.listItems = this.listItems.concat(this.listFor);
    }
  }

  fillEmptyListItems() {
    for (let i = 0; i < this.listItems.length; i++) {
      if (this.listItems[i].string == "") {
        this.listItems[i].string = "name " + i;
      }
      if (this.listItems[i].number == "") {
        this.listItems[i].number = i;
      }
    }
    //console.log(this.listItems[5].string);
    console.log("listItems:");
    console.table(this.listItems);
    this.saveRunners();
  }

  async saveRunners() {
    var i = 0;
    this.listItems.forEach(el => {
      var runner: RunnersRow = {
        startNumber: i + 1,
        name: el.string,
        tshirtNumber: el.number,
        startTime: this.listIntervals[i],
        finishedTime: "",
        checkpoints: [""]
      };
      //console.log(runner);
      this.runners.push(runner);
      i++;
    });
    console.log("runners:");
    console.table(this.runners);
    await this.fileMenager.writeFile(this.runners, this.raceinfo.file);
    this.router.navigate([
      "/raceinfoscreen",
      { race: JSON.stringify(this.raceinfo) }
    ]);
  }

  // Function for reordering items.
  // Also displays reordering in the console as it occurs
  reorderItems(event) {
    console.log(`Moving item from ${event.detail.from} to ${event.detail.to}`);

    const itemMove = this.listItems.splice(event.detail.from, 1)[0];
    this.listItems.splice(event.detail.to, 0, itemMove);
    event.detail.complete();
  }

  numberFromInput(i, event) {
    this.listItems[i].number = event.target.value;
    //console.log(this.listItems.number);
  }
  nameFromInput(i, event) {
    this.listItems[i].string = event.target.value;
    //event.target.value.complete();
  }
  // WIP, Call this to access info?
  getList() {
    console.table(this.listItems);
  }

  ngOnInit() {
    this.createInterval();
    this.createList();
  }
}
