import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewRaceListPage } from './new-race-list.page';

describe('NewRaceListPage', () => {
  let component: NewRaceListPage;
  let fixture: ComponentFixture<NewRaceListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewRaceListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewRaceListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
