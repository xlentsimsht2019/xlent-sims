import { HomePage } from './../home/home.page';

import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss'],
})
export class BaseComponent implements OnInit {
  
  constructor() {}
  
  ngOnInit() {
  }

}
