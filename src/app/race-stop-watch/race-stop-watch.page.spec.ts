import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RaceStopWatchPage } from './race-stop-watch.page';

describe('RaceStopWatchPage', () => {
  let component: RaceStopWatchPage;
  let fixture: ComponentFixture<RaceStopWatchPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RaceStopWatchPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RaceStopWatchPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
