import { Routes, Router } from "@angular/router";
import { AppRoutingModule } from "./../app-routing.module";
import {
  FileMenagerService,
  RunnersRow
} from "./../services/file-menager.service";
import {
  StorageMenagerService,
  Race
} from "./../services/storage-menager.service";
import { Component, OnInit } from "@angular/core";
import { NavigationEnd } from "@angular/router";

@Component({
  selector: "app-past-race",
  templateUrl: "./past-race.page.html",
  styleUrls: ["./past-race.page.scss"]
})
export class PastRacePage implements OnInit {
  races: Race[] = [];

  constructor(
    private storageService: StorageMenagerService,
    private fileMenager: FileMenagerService,
    private router: Router
  ) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const routePath = event.url.split('/')[1];
        this.races = this.loadRaces();
        if (routePath === 'pastrace') {
          this.races = this.loadRaces();
        }
      }
    });
  }

  /* Load all the races, finished and not */
  loadRaces(): Race[] {
    this.storageService.getRaces().then(races => {
      if (races != null) {
        this.races = races;
        this.fileMenager.readFile(this.races[0].file).then(runners => {
          var Runners: RunnersRow[] = runners;
          console.log(Runners);
        });
      } else {
        this.races = [];
      }
    });
    return this.races;
  }

  getInfo(race) {
    console.log(race);
    // We have to redirect the user to the race page
  }

  deleteRace(race) {
    this.storageService.deleteRace(race.id);
    this.races = this.loadRaces();
    // We have to show a prompt and try to remove the race
  }

  ngOnInit() {
    this.races = this.loadRaces();
  }
}
