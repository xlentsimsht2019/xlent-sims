import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastRacePage } from './past-race.page';

describe('PastRacePage', () => {
  let component: PastRacePage;
  let fixture: ComponentFixture<PastRacePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastRacePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastRacePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
