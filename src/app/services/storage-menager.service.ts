import { HomePage } from './../home/home.page';
import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { BehaviorSubject, Observable } from 'rxjs';
import { FileMenagerService, RunnersRow } from './file-menager.service';

export interface Race {
  id: number;
  name?: string;
  date?: Date;
  interval?: number;
  raceType: string;
  numberOfRunners: number;
  file?: string;
  finishflag: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class StorageMenagerService {
  private db: SQLiteObject;
  private dbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);
  races: Race[] = [];
  options: any = {
    name: 'XLENTiming.db',
    location: 'default'
  };

  constructor(
    private plt: Platform,
    private sqlite: SQLite,
    private fileMenager: FileMenagerService,
    private storageService: StorageMenagerService
  ) {
    this.plt.ready().then(() => {
      this.sqlite
        .create(this.options)
        .then((database: SQLiteObject) => {
          this.db = database;
          const raceTable =
            'CREATE TABLE IF NOT EXISTS race (id INTEGER PRIMARY KEY, ' +
            'name TEXT, date TEXT,interval INTEGER,raceType nTEXT, ' +
            'n_departure INTEGER,file TEXT,finishflag TEXT);';
          // IF you move the below statement out of here then the db variable
          // will not be initialized before you can use it to execute the SQL.
          this.db
            .executeSql(raceTable, [])
            .then(() => {
              console.log('Table Created!');
            })
            .catch(e => {
              console.error(e);
            });
          this.dbReady.next(true);
        })
        .catch(e => console.error('Qualcosa non è andato'));
    });
  }

  addRace(race: Race): Promise<any> {
    const data = [
      race.id,
      race.name,
      race.date,
      race.interval,
      race.raceType,
      race.numberOfRunners,
      race.file,
      race.finishflag
    ];

    console.log('Add Race Method Called. Trying to add the race...');
    console.log('The Race to be added is...');
    console.log(race);

    const insertRace =
      'INSERT INTO race (id, name, date, interval, raceType, n_departure, file, finishflag) VALUES (?,?,?,?,?,?,?,?);';

    return this.db
      .executeSql(insertRace, data)
      .then(() => {
        console.log('Insert Complete!');
        this.loadRaces();
      })
      .catch(e => {
        console.log(e);
      });
  }

  deleteRace(id) {
    const deleteQuery = 'DELETE FROM race WHERE id = ?;';
    return this.db
      .executeSql(deleteQuery, [id])
      .then(() => {
        console.log('Delete Complete!');
        this.loadRaces();
      })
      .catch(e => {
        console.log(e);
      });
  }

  /* Returns all the races, completed and not completed */
  getRaces(): Promise<any> {
    const selectRace = 'SELECT * FROM race ORDER BY date DESC';
    return this.db
      .executeSql(selectRace, [])
      .then(result => {
        const races: Race[] = [];
        if (result.rows.length > 0) {
          for (let i = 0; i < result.rows.length; i++) {
            races.push(result.rows.item(i));
          }
        }
        return races;
      })
      .catch(err => {
        return console.error(err);
      });
  }

  /* Returns last three unfinished races */
  getLastUnfinishedRace(): Promise<any> {
    console.log('Loading 3 last unfinished races');
    let only3races = [];
    const selectRace = 'SELECT * FROM race WHERE finishflag = \'false\'';
    return this.db
      .executeSql(selectRace, [])
      .then(result => {
        const races: Race[] = [];
        if (result.rows.length > 0) {
          for (let i = 0; i < result.rows.length; i++) {
            races.push(result.rows.item(i));
          }
        }
        if (races.length > 3) {
          only3races[0] = races[races.length - 1];
          only3races[1] = races[races.length - 2];
          only3races[2] = races[races.length - 3];
        } else {
          only3races = races;
        }
        console.log('Number of races in DB: ' + races.length);
        return only3races;
      })
      .catch(err => {
        console.log(err);
        return only3races;
      });
  }

  /* Returns database state */
  getDatabaseState() {
    return this.dbReady.asObservable();
  }

  /* Load last three unfinished races */
  loadRaces() {
    this.storageService.getLastUnfinishedRace().then(races => {
      if (races != null) {
        this.races = races;
        console.log(this.races);
        this.fileMenager.readFile(this.races[0].file).then(runners => {
          const Runners: RunnersRow[] = runners;
          console.log(Runners);
        });
      }
    });
  }
}
