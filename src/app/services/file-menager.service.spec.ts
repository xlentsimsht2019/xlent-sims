import { TestBed } from '@angular/core/testing';

import { FileMenagerService } from './file-menager.service';

describe('FileMenagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FileMenagerService = TestBed.get(FileMenagerService);
    expect(service).toBeTruthy();
  });
});
