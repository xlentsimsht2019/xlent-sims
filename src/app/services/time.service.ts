import { Injectable } from "@angular/core";

export interface RunnerTime {
  hour: number;
  minute: number;
  second: number;
  millisec: number;
}

@Injectable({
  providedIn: "root"
})
export class TimeService {
  constructor() {}

  parseStringToTime() {
    // to implement
  }

  parseTimeToString() {
    //to implement
  }
}
