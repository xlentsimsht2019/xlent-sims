import { Injectable } from "@angular/core";
import { File } from "@ionic-native/file/ngx";
import { stringify } from "querystring";

export interface RunnersRow {
  startNumber: number;
  name: string;
  tshirtNumber: number;
  startTime: string;
  finishedTime: string;
  checkpoints: string[];
}

@Injectable({
  providedIn: "root"
})
export class FileMenagerService {
  pathfile: string;

  constructor(private file: File) {
    this.pathfile = this.file.dataDirectory;
  }

  createNewFile(filename: string) {
    console.log(filename);
    this.file
      .checkFile(this.pathfile, filename)
      .then(doesExist => {
        console.log("doesExist : " + doesExist);
      })
      .catch(err => {
        return this.file
          .createFile(this.file.dataDirectory, filename, true)
          .then(FileEntry => {
            console.log("File created!");
          })
          .catch(err => console.log(err));
      });
  }

  async writeFile(runners: RunnersRow[], filename: string): Promise<any> {
    var text = "";

    runners.forEach(runner => {
      text =
        text +
        runner.startNumber +
        "," +
        runner.name +
        "," +
        runner.startTime +
        "," +
        runner.tshirtNumber +
        "," +
        runner.finishedTime +
        "," +
        "[" +
        runner.checkpoints +
        "]" +
        ";\n";
    });
    console.log(text);
    var writer = this.file.writeFile(this.file.dataDirectory, filename, text, {
      replace: true,
      append: false
    });

    await writer
      .then(txt => {
        console.log("Funziona");
      })
      .catch(err => {
        console.log(err);
      });
    //var x = this.readFile(filename);
    //console.log(x);
    return;
  }

  async readFile(filename: string): Promise<RunnersRow[]> {
    var runners: RunnersRow[] = [];
    var promise = this.file.readAsText(this.file.dataDirectory, filename);
    return await promise
      .then(txtread => {
        console.log(txtread);
        if (typeof txtread == "string") {
          return this.parseFile(txtread);
        } else {
          console.log("I cant open this file");
          return runners;
        }
      })
      .catch(err => {
        console.log(err);
        return runners;
      });
  }
  parseFile(text: string): RunnersRow[] {
    //console.log(text);
    var runners: RunnersRow[] = [];
    var RunnersRowFields = [];
    var checkpoint = [];
    var i = 0;
    var j = 0;
    var k = 0;
    RunnersRowFields[0] = "";
    checkpoint[0] = "";
    var runner: RunnersRow;

    while (i < text.length) {
      //console.log(text[i]);
      if (text.charAt(i) != ";") {
        if (text.charAt(i) != ",") {
          if (text.charAt(i) == "[") {
            i++;
            k = 0;
            while (text.charAt(i) != "]") {
              if (text.charAt(i) != ",") {
                checkpoint[k] = checkpoint[k] + text.charAt(i);
              } else {
                k = k + 1;
                checkpoint[k] = "";
              }
              i++;
            }
          } else {
            RunnersRowFields[j] = RunnersRowFields[j] + text.charAt(i);
          }
        } else {
          j = j + 1;
          RunnersRowFields[j] = "";
        }
      } else {
        // console.log(RunnersRowFields);
        //console.log(checkpoint);
        runner = {
          startNumber: RunnersRowFields[0],
          name: RunnersRowFields[1],
          startTime: RunnersRowFields[2],
          tshirtNumber: RunnersRowFields[3],
          finishedTime: RunnersRowFields[4],
          checkpoints: checkpoint
        };
        // console.log(runner);
        runners.push(runner);
        runner = {} as RunnersRow;
        RunnersRowFields = [];
        checkpoint = [];
        RunnersRowFields[0] = "";
        j = 0;
        i++;
      }
      i++;
    }
    return runners;
  }
}
