import { TestBed } from '@angular/core/testing';

import { StorageMenagerService } from './storage-menager.service';

describe('StorageMenagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StorageMenagerService = TestBed.get(StorageMenagerService);
    expect(service).toBeTruthy();
  });
});
