import { Component, OnInit, OnChanges } from "@angular/core";
import {
  StorageMenagerService,
  Race
} from "../services/storage-menager.service";
import { Platform, ToastController } from "@ionic/angular";
import {
  RunnersRow,
  FileMenagerService
} from "../services/file-menager.service";
import { Router, NavigationEnd } from "@angular/router";

@Component({
  selector: "app-home",
  templateUrl: "./home.page.html",
  styleUrls: ["./home.page.scss"]
})
export class HomePage implements OnInit {
  races: Race[] = [];
  path: string;
  newRace: Race = <Race>{};

  constructor(
    private storageService: StorageMenagerService,
    private plt: Platform,
    private toastController: ToastController,
    private fileMenager: FileMenagerService,
    private router: Router
  ) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const routePath = event.url.split("/")[1];
        if (routePath === "home") {
          this.races = this.loadRaces();
        }
      }
    });
  }

  loadRaces(): Race[] {
    this.storageService.getLastUnfinishedRace().then(races => {
      if (races.length !== 0) {
        this.races = races;
        this.fileMenager.readFile(this.races[0].file).then(runners => {
          var Runners: RunnersRow[] = runners;
          console.log(Runners);
        });
      } else {
        this.races = [];
      }
      console.log(races.length);
    });
    return this.races;
  }

  async showToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 1500
    });
    toast.present();
  }

  getInfo(race) {
    console.log(race.id);
    console.log(race);
    // We have to redirect the user to the race page
  }

  deleteRace(race) {
    console.log(race.id);
    console.log(race);
    console.log("Trying to delete a race...");
    this.storageService.deleteRace(race.id);
    console.log("Race deleted!!!");
    this.races = this.loadRaces();
    // We have to show a prompt and try to remove the race
  }

  ngOnInit() {
    this.storageService.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.races = this.loadRaces();
      }
    });
  }
}
