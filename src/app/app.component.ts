import { Router } from '@angular/router';
import { StorageMenagerService } from './services/storage-menager.service';
import { Component } from '@angular/core';

import { Platform, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})

export class AppComponent {
  title = 'XLENT Timing';
  routerLocal = '';


  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storageService: StorageMenagerService,
    private menu: MenuController,
    public router: Router
  ) {
    this.initializeApp();
    this.routerLocal = router.url;
  }

  updateRaces() {
    this.storageService.getRaces();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  toggleMenu() {
    this.menu.toggle();
  }

}
