import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { BaseComponent } from './base/base.component';
import { HomePageModule } from './home/home.module';
import { NewRaceSetupPageModule } from './new-race-setup/new-race-setup.module';
import { NewRaceListPageModule } from './new-race-list/new-race-list.module';
import { RaceStopWatchPageModule } from './race-stop-watch/race-stop-watch.module';
import { RaceInfoScreenPageModule } from './race-info-screen/race-info-screen.module';
import { PastRacePageModule } from './past-race/past-race.module';
import { MenuPageModule } from './menu/menu.module';
import { AboutPageModule } from './about/about.module';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => HomePageModule
  },
  {
    path: 'pastrace',
    loadChildren: () => PastRacePageModule
  },
  {
    path: 'racesetup',
    loadChildren: () => NewRaceSetupPageModule
  },
  {
    path: 'newracelist',
    loadChildren: () => NewRaceListPageModule
  },
  {
    path: 'raceinfoscreen',
    loadChildren: () => RaceInfoScreenPageModule
  },
  {
    path: 'racestopwatch',
    loadChildren: () => RaceStopWatchPageModule
  },
  {
    path: 'menu',
    loadChildren: () => MenuPageModule
  },
  {
    path: 'about',
    loadChildren: () => AboutPageModule
  },
  {
    path: '',
    pathMatch: 'prefix',
    redirectTo: 'home'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
