import { Component, OnInit } from "@angular/core";
import {
  FileMenagerService,
  RunnersRow
} from "../services/file-menager.service";
import { NewRaceSetupPage } from "../new-race-setup/new-race-setup.page";
import { NewRaceListPage } from "../new-race-list/new-race-list.page";
import {
  StorageMenagerService,
  Race
} from "../services/storage-menager.service";
import { Platform } from "@ionic/angular";
import { ActivatedRoute } from "@angular/router";
//import { writeFile } from 'fs';
import { Router, NavigationEnd } from "@angular/router";
import { stringify } from "querystring";
import { TargetLocator } from "selenium-webdriver";

//import { readFileSync } from 'fs';

//REMOVE REDUNDANT IMPORTS LATER!!

@Component({
  selector: "app-race-info-screen",
  templateUrl: "./race-info-screen.page.html",
  styleUrls: ["./race-info-screen.page.scss"]
})
export class RaceInfoScreenPage implements OnInit {
  tempListItems: any;
  intervalIncrements: any;
  listFor: any;
  participants: any;
  type: string;
  startTime: any;
  intervalTime: any;
  intervalToNumber: number;
  date: any;
  runners: RunnersRow[] = [];
  viewRaceInfo: Race;
  //startTimeToNumber: number;

  constructor(
    private file: FileMenagerService,
    private storageService: StorageMenagerService,
    private plt: Platform,
    private fileMenager: FileMenagerService,
    private activeroute: ActivatedRoute,
    private router: Router
  ) {
    this.viewRaceInfo = JSON.parse(
      this.activeroute.snapshot.paramMap.get("race")
    );
    console.log("RaceInfoScreen: raceinfo");
    console.log(this.viewRaceInfo);
  }

  async loadfile(filename): Promise<any> {
    console.log("loadfile");
    var x = this.fileMenager.readFile(filename);
    await x.then(runners => {
      this.runners = runners;
    });
    //console.log(this.runners);
    return;
  }
  createRaceInfo() {
    this.participants = this.viewRaceInfo.numberOfRunners;
    this.type = this.viewRaceInfo.raceType;
    /*this.startTime =  this.viewRaceInfo.date.getDay() + ":" +
                      this.viewRaceInfo.date.getHours() + ":" +
                      this.viewRaceInfo.date.getMinutes() + ":" +
                      this.viewRaceInfo.date.getSeconds();*/
    //this.startTime = this.viewRaceInfo.date;
    this.date = this.viewRaceInfo.date;
    this.intervalTime = this.viewRaceInfo.interval;

    var t = this.date.split(/[- : T +]/);
    var d = new Date(
      parseInt(t[0]),
      parseInt(t[1]) - 1,
      parseInt(t[2]),
      parseInt(t[3]),
      parseInt(t[4]),
      parseInt(t[5]),
      parseInt(t[6])
    );
    var IntervalToDate = new Date(d);
    this.startTime = new Date(d);
    /*this.startTime =  this.startTime.getDay() + ":" +
                      this.startTime.getHours() + ":" +
                      this.startTime.getMinutes() + ":" +
                      this.startTime.date.getSeconds();*/
    //console.log(IntervalToDate);

    this.tempListItems = [];
    this.listFor = [];
    this.intervalIncrements = [];

    this.intervalToNumber = Number(this.intervalTime);

    //An array of each incremented value. Unsure if this is needed
    //console.log("intervalToDate:");
    for (let i = 0; i < this.participants; i++) {
      this.intervalIncrements.push(
        this.intervalToNumber + this.intervalToNumber * i
      );
    }
    //console.log(this.intervalIncrements);

    // Inserts the names and numbers into listItems
    if (this.type == "individual") {
      for (let i = 0; i < this.participants; i++) {
        this.listFor = [
          {
            string: this.runners[i].name,
            number: this.runners[i].tshirtNumber,
            interval: this.runners[i].startTime.replace(".00", "") // omits the miliseconds
          }
        ];

        IntervalToDate.setSeconds(
          IntervalToDate.getSeconds() + this.intervalToNumber
        );
        this.tempListItems = this.tempListItems.concat(this.listFor);
        //console.log(this.tempListItems[i].interval);
      }
    }
  }

  async ngOnInit() {
    await this.loadfile(this.viewRaceInfo.file);
    console.log("ngOnit");
    console.log(this.runners);
    this.createRaceInfo();
  }
}
