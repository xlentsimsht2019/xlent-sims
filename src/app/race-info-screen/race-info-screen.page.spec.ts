import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RaceInfoScreenPage } from './race-info-screen.page';

describe('RaceInfoScreenPage', () => {
  let component: RaceInfoScreenPage;
  let fixture: ComponentFixture<RaceInfoScreenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RaceInfoScreenPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RaceInfoScreenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
