import { Router, NavigationEnd } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.page.html',
  styleUrls: ['./footer.page.scss']
})
export class FooterPage implements OnInit {

  routerLocal = '';

  constructor(private router: Router) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const routePath = event.url.split('/')[1];
        this.routerLocal = routePath;
      }
    });
  }

  navigateToHome() {
    this.router.navigate(['/home']);
  }

  navigateToPastRace() {
    this.router.navigate(['/pastrace']);
  }

  navigateToAbout() {
    this.router.navigate(['/about']);
  }

  ngOnInit() {
    this.routerLocal = this.router.url.split('/')[1];
  }
}
