import { Component, OnInit } from "@angular/core";
import { Storage } from "@ionic/storage";

@Component({
  selector: "app-timer",
  templateUrl: "./timer.component.html",
  styleUrls: ["./timer.component.scss"]
})
export class TimerComponent implements OnInit {
  public timeBegan = null;
  public timeStopped: any = null;
  public stoppedDuration: any = 0;
  public started = null;
  public running = false;
  public blankTime = "00:00.000";
  public time = "00:00.000";
  public laps: string[] = [];

  constructor() {}

  start() {
    if (this.running) return;
    if (this.timeBegan === null) {
      this.reset();
      this.timeBegan = new Date();
    }
    if (this.timeStopped !== null) {
      let newStoppedDuration: any = +new Date() - this.timeStopped;
      this.stoppedDuration = this.stoppedDuration + newStoppedDuration;
    }
    this.started = setInterval(this.clockRunning.bind(this), 10);
    this.running = true;
  }
  stop() {
    this.running = false;
    this.timeStopped = new Date();
    clearInterval(this.started);
  }
  reset() {
    this.running = false;
    clearInterval(this.started);
    this.stoppedDuration = 0;
    this.timeBegan = null;
    this.timeStopped = null;
    this.time = this.blankTime;
  }
  zeroPrefix(num, digit) {
    let zero = "";
    for (let i = 0; i < digit; i++) {
      zero += "0";
    }
    return (zero + num).slice(-digit);
  }
  clockRunning() {
    let currentTime: any = new Date();
    let timeElapsed: any = new Date(
      currentTime - this.timeBegan - this.stoppedDuration
    );

    this.time = this.parseTimeToString(timeElapsed);
  }

  parseTimeToString(timeElapsed: Date) {
    let hour = timeElapsed.getUTCHours();
    let min = timeElapsed.getUTCMinutes();
    let sec = timeElapsed.getUTCSeconds();
    let ms = timeElapsed.getUTCMilliseconds();
    var timestring =
      this.zeroPrefix(hour, 2) +
      ":" +
      this.zeroPrefix(min, 2) +
      ":" +
      this.zeroPrefix(sec, 2) +
      "." +
      this.zeroPrefix(ms, 3);

    return timestring;
  }
  lap() {
    let currentTime: any = new Date();
    let lap: any = new Date(
      currentTime - this.timeBegan - this.stoppedDuration
    );
    var lapstring = this.parseTimeToString(lap);
    this.laps.push(lapstring);
  }

  ngOnInit() {}
}
